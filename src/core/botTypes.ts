export interface InvisibleError {
    hiddenError: Error;
    loggerMsg: string;
}

export interface VisibleError {
    visibleMsg: string;
    error: Error;
}

export interface Response {
    responseMsg: string;
}

export type ReactType = "heavy_check_mark" | "x";

export type ReactReply = {
    react: ReactType;
};

/**
 * These are the types of reply a bot can give to a user command:
 * 1. Response       : Usual return, everything went well. Just post this in the same channel.
 * 2. ReactReply     : Simple yes/no response to a user command
 * 3. VisibleError   : Something went wrong processing, probably due to user error
 * 4. InvisibleError : Something went wrong processing, probably due to server error.
 * 5. null           : No response
 * Don't display this but TODO: send a stacktrace to an admin.
 */
export type BotReply = Response | ReactReply | null;
export type BotError = VisibleError | InvisibleError;

export type Timestamp = string;

export function isResponse(reply: BotReply): reply is Response {
    return (reply as Response).responseMsg !== undefined;
}

export function isReact(reply: BotReply): reply is ReactReply {
    return (reply as ReactReply).react !== undefined;
}

export function isVisibleError(reply: BotError): reply is VisibleError {
    return (reply as VisibleError).visibleMsg !== undefined;
}

export function isInvisibleError(reply: BotError): reply is InvisibleError {
    return (reply as InvisibleError).hiddenError !== undefined;
}

export function assertNever(_x: never): never {
    throw new Error(`Unexpected object`);
}

export function invisErr(loggerMsg: string, error: Error): InvisibleError {
    return { hiddenError: error, loggerMsg: loggerMsg } as InvisibleError;
}

export function visErr(errMsg: string): VisibleError {
    return { visibleMsg: errMsg } as VisibleError;
}

export function resp(msg: string): Response {
    return { responseMsg: msg } as Response;
}

export function react(reactType: ReactType): ReactReply {
    return { react: reactType } as ReactReply;
}
