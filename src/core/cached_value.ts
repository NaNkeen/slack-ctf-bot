import { Result, ok } from "neverthrow";
import { logger } from "./config";

export type CacheValueFunction<M> = () => Promise<Result<M, Error>>;

export class CachedValueStore<T> {
    public static getDefaultCache<K>(
        cacheName: string,
        getValue: CacheValueFunction<K>
    ): CachedValueStore<K> {
        return new CachedValueStore<K>(
            this.DEFAULT_CACHE_LIMIT,
            cacheName,
            getValue
        );
    }

    private static readonly DEFAULT_CACHE_LIMIT = 20;
    private value: T | null;
    private getValue: CacheValueFunction<T>;
    private cacheLimit: number;
    private numReads: number;
    private cacheName: string;
    private invalid: boolean;

    constructor(
        cacheLimit: number,
        cacheName: string,
        getValue: CacheValueFunction<T>
    ) {
        this.value = null;
        this.getValue = getValue;
        this.cacheLimit = cacheLimit;
        this.numReads = 0;
        this.cacheName = cacheName;
        this.invalid = false;
    }

    public async forceCacheRefresh(): Promise<Result<T, Error>> {
        return await this.refreshCache();
    }

    public async getCachedValue(): Promise<Result<T, Error>> {
        if (this.value != null && !this.invalid) {
            if (this.numReads > this.cacheLimit - 1) {
                this.invalid = true;
                logger.info(`Cache "${this.cacheName}" is invalid! Refreshing`);
            }
            this.numReads += 1;
            return ok(this.value);
        } else {
            const val = await this.refreshCache();
            this.invalid = false;
            return val;
        }
    }

    private async refreshCache(): Promise<Result<T, Error>> {
        const newValue = await this.getValue();

        this.numReads = 0;

        const retValue = newValue.map((okVal) => {
            this.invalid = false;
            this.value = okVal;
            return okVal;
        });

        return retValue;
    }

    public invalidateCache(): void {
        logger.info(`Invalidated Cache ${this.cacheName}`);
        this.invalid = true;
    }
}
