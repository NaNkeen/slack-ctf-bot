export const getEpochTime = (): number => {
    return Math.floor(Date.now() / 1000);
};
