import { SlackAPI } from "../slack_interface/slack_api";
import { SlackChannel } from "../slack_interface/channel";
import { SlackUser } from "../slack_interface/user";
import { Result } from "neverthrow";
import { Timestamp, BotReply, BotError } from "./botTypes";
import { Bot } from "./bot";

export type CommandFunctionAsync = (
    slackAPI: SlackAPI,
    command: BotCommand,
    bot: Bot,
    channelId: SlackChannel,
    timestamp: Timestamp,
    userId: SlackUser,
    args: string[]
) => Promise<Result<BotReply, BotError>>;

export type CommandFunction = (
    slackAPI: SlackAPI,
    command: BotCommand,
    bot: Bot,
    channelId: SlackChannel,
    timestamp: Timestamp,
    userId: SlackUser,
    args: string[]
) => Result<BotReply, BotError>;

export interface BotCommand {
    name: string;
    callback: CommandFunctionAsync | CommandFunction;
    description: string;
    usage: string;
}
