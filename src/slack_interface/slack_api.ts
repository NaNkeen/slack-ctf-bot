import { WebClient, LogLevel, WebAPICallResult } from "@slack/web-api";
import { SlackChannel } from "./channel";
import { SlackChannelManager } from "./channel_manager";
import { Result, ok, err } from "neverthrow";
import { SlackUserManager } from "./user_manager";
import { SlackUser } from "./user";
import { Timestamp, ReactType } from "../core/botTypes";
import { WebResult } from "./rtm_types";
import { logger } from "../core/config";

//TODO: Proper chat interface to change our messaging platform?
export class SlackAPI {
    public static init(token: string): SlackAPI {
        const web = new WebClient(token, {
            logLevel: LogLevel.INFO,
        });
        const channelManager = new SlackChannelManager(web);
        const userManager = new SlackUserManager(web);

        return new SlackAPI(web, channelManager, userManager);
    }

    private web: WebClient;
    private channelManager: SlackChannelManager;
    private userManager: SlackUserManager;

    constructor(
        web: WebClient,
        channelManager: SlackChannelManager,
        userManager: SlackUserManager
    ) {
        this.web = web;
        this.channelManager = channelManager;
        this.userManager = userManager;
    }

    //TODO: Maybe move this?
    public postMessageToChannel(
        message: string,
        channel: SlackChannel
    ): Result<null, Error> {
        this.web.chat
            .postMessage({
                channel: channel.getId(),
                text: message,
            })
            .catch((error) => {
                return err(error);
            });
        return ok(null);
    }

    public async archiveChannel(
        channel: SlackChannel
    ): Promise<Result<null, Error>> {
        return this.channelManager.archiveChannel(channel);
    }

    public async getChannelById(
        id: string
    ): Promise<Result<SlackChannel, Error>> {
        return await this.channelManager.getChannelById(id);
    }

    private static async getCursorDataInner<T extends WebAPICallResult>(
        request: (cursor: string | undefined) => Promise<T>,
        prev_results: Array<T>,
        cursor: string | undefined
    ): Promise<Array<T>> {
        const result = await request(cursor);
        const new_results = [...prev_results, result];

        // I hate JS
        if (
            result.response_metadata == null ||
            result.response_metadata.next_cursor == null ||
            result.response_metadata.next_cursor == ""
        ) {
            // No more data return
            return new_results;
        }

        return this.getCursorDataInner(
            request,
            new_results,
            result.response_metadata.next_cursor
        );
    }

    public static async getCursorData<T extends WebAPICallResult>(
        request: (cursor: string | undefined) => Promise<T>
    ): Promise<Array<T>> {
        return this.getCursorDataInner(request, [], undefined);
    }

    public async getUsers(): Promise<Result<Map<string, SlackUser>, Error>> {
        return await this.userManager.getUsers();
    }

    public async getUserById(id: string): Promise<Result<SlackUser, Error>> {
        return this.userManager.getUserById(id);
    }

    public async reactToMessage(
        channel: SlackChannel,
        timestamp: Timestamp,
        reactType: ReactType
    ): Promise<Result<null, Error>> {
        const result: WebResult = (await this.web.reactions.add({
            timestamp: timestamp,
            name: reactType,
            channel: channel.getId(),
        })) as WebResult;

        if (!result.ok) {
            const errorString = result.error ? result.error : "Unknown Error";
            const error = Error(errorString);
            logger.error(
                `Error occured reacting to message in channel ${channel.getName()}`,
                error
            );
            return err(error);
        } else {
            return ok(null);
        }
    }

    public async createPublicChannel(
        name: string
    ): Promise<Result<SlackChannel, Error>> {
        return this.channelManager.createChannel(name, false);
    }

    public async createChannel(
        name: string,
        isPrivate: boolean
    ): Promise<Result<SlackChannel, Error>> {
        return this.channelManager.createChannel(name, isPrivate);
    }

    public async invitetoChannel(
        channel: SlackChannel,
        user: SlackUser
    ): Promise<void> {
        await this.web.conversations.invite({
            channel: channel.getId(),
            users: user.id,
        });
    }
}
