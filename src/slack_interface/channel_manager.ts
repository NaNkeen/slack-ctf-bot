import { SlackChannel } from "./channel";
import { WebClient } from "@slack/web-api";
import {
    Channel,
    ChannelListing,
    WebResult,
    ChannelCreateResult,
} from "./rtm_types";
import { Result, ok, err } from "neverthrow";
import { CachedValueStore } from "../core/cached_value";
import { logger } from "../core/config";
import { SlackAPI } from "./slack_api";

export class SlackChannelManager {
    private static CHANNEL_CACHE_LIMIT = 100;

    private channels: CachedValueStore<Map<string, Channel>>;
    private web: WebClient;

    constructor(web: WebClient) {
        this.channels = new CachedValueStore(
            SlackChannelManager.CHANNEL_CACHE_LIMIT,
            "Channel Cache",
            () => this.getChannels()
        );

        this.web = web;
    }

    public async getChannelById(
        id: string
    ): Promise<Result<SlackChannel, Error>> {
        return (await this.channels.getCachedValue()).andThen((channels) => {
            const channelInfo = channels.get(id);

            if (channelInfo) {
                return ok(new SlackChannel(channelInfo));
            } else {
                return err(Error(`Couldn't find channel with id: ${id}`));
            }
        });
    }

    private async getChannels(): Promise<Result<Map<string, Channel>, Error>> {
        try {
            const channel_request = (
                cursor: string | undefined
            ): Promise<ChannelListing> => {
                return this.web.conversations.list({
                    types: "public_channel,private_channel",
                    cursor: cursor,
                    exclude_archived: true,
                }) as Promise<ChannelListing>;
            };

            const channelListings: ChannelListing[] = await SlackAPI.getCursorData(
                channel_request
            );
            const channels = channelListings.reduce(
                (res: Array<Channel>, channelListing) => {
                    return res.concat(channelListing.channels);
                },
                []
            );

            return ok(
                new Map(
                    channels.map((channel) => {
                        return [channel.id, channel];
                    })
                )
            );
        } catch (e) {
            return err(e);
        }
    }

    public async archiveChannel(
        channel: SlackChannel
    ): Promise<Result<null, Error>> {
        try {
            const result = (await this.web.conversations.archive({
                channel: channel.getId(),
            })) as WebResult;

            if (result.ok) {
                return ok(null);
            } else {
                const error = Error(result.error);
                logger.error(
                    `Deleting channel ${channel.getName()} failed!`,
                    error
                );
                return err(error);
            }
        } catch (error) {
            logger.error(
                `Deleting channel ${channel.getName()} failed!`,
                error
            );
            return err(error);
        }
    }

    public async createChannel(
        name: string,
        isPrivate: boolean
    ): Promise<Result<SlackChannel, Error>> {
        try {
            const chan = (await this.web.conversations.create({
                name: name,
                is_private: isPrivate,
            })) as ChannelCreateResult;

            if (chan.error) {
                const error = Error(chan.error);
                logger.error(`Failed to create channel ${name}`, error);
                return err(error);
            } else if (chan.channel) {
                logger.info(
                    `Created channel ${chan.channel.name}, id:${chan.channel.id}`
                );
                await this.channels.forceCacheRefresh();
                return ok(new SlackChannel(chan.channel));
            } else {
                const error = Error(
                    "Wut? Should always have either an error or a channel now :("
                );
                return err(error);
            }
        } catch (e) {
            return err(e);
        }
    }
}
