import { CTFDB } from "./db/CTFDB";
import { WorkerProps } from "@prisma/client";
import { Challenge } from "./Challenge";
import { SlackUser } from "../../slack_interface/user";
import { Result } from "neverthrow";

export class ChallengeWorker implements WorkerProps {
    private db: CTFDB;
    private props: WorkerProps;

    private constructor(db: CTFDB, props: WorkerProps) {
        this.db = db;
        this.props = props;
    }

    get rowid(): number {
        return this.props.rowid;
    }

    get challengeId(): number {
        return this.props.challengeId;
    }
    get chatId(): string {
        return this.props.chatId;
    }

    get solved(): number {
        return this.props.solved;
    }

    get isSolved(): boolean {
        return this.props.solved == 1;
    }

    public static async addWorkerToChallenge(
        db: CTFDB,
        challenge: Challenge,
        user: SlackUser
    ): Promise<Result<ChallengeWorker, Error>> {
        return (await db.addWorker(challenge, user)).map((props) => {
            return new this(db, props);
        });
    }
    public static async workersOnChallenge(
        db: CTFDB,
        challenge: Challenge
    ): Promise<Array<ChallengeWorker>> {
        return (await db.getWorkerProps(challenge)).map((workerProp) => {
            return new this(db, workerProp);
        });
    }
}
