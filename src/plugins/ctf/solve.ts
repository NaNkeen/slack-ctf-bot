import { Challenge } from "./Challenge";
import { SlackChannel } from "../../slack_interface/channel";
import { postToGeneralChannel } from "./ctf_bot";
import { CTFDB } from "./db/CTFDB";
import { err, Result, ok } from "neverthrow";
import { visErr, BotError, BotReply, resp } from "../../core/botTypes";
import { SlackAPI } from "../../slack_interface/slack_api";
import { SlackUser } from "../../slack_interface/user";

export async function doSolveByChannel(
    db: CTFDB,
    api: SlackAPI,
    channel: SlackChannel,
    user: SlackUser
): Promise<Result<BotReply, BotError>> {
    const challengeOrErr = await Challenge.getChallengeFromChannel(db, channel);

    if (challengeOrErr.isErr()) {
        return err(visErr("Couldn't find this challenge channel!"));
    }

    const challenge = challengeOrErr.value;

    if (await challenge.isSolved()) {
        return err(visErr(`Can't solve a challenge that's already solved!`));
    }

    await solveChallenge(db, api, challenge, user);

    return ok(resp(`Marked solved! Thank you ${user.real_name}`));
}

export async function solveChallenge(
    db: CTFDB,
    api: SlackAPI,
    challenge: Challenge,
    user: SlackUser
): Promise<void> {
    //TODO: Multiple solvers
    await challenge.markSolved(new Array(user));

    await postToGeneralChannel(
        db,
        api,
        challenge,
        `:tada: <!here> ${user.real_name} has solved challenge ${challenge.name}! :tada:`
    );
}
