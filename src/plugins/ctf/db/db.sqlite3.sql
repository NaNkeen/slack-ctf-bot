BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "Challenge" (
    "rowid"             INTEGER PRIMARY KEY,
    "name"              TEXT NOT NULL,
    "slack_channel_id"  TEXT NOT NULL UNIQUE,
    "type"              TEXT NOT NULL,
    "ctf_id"            INTEGER NOT NULL,
    FOREIGN KEY("ctf_id") REFERENCES "CTF"("rowid")
);

CREATE TABLE IF NOT EXISTS "Worker" (
    "rowid"        INTEGER PRIMARY KEY,
    "chat_id"      TEXT NOT NULL,
    "challenge"    INTEGER NOT NULL,
    "solved"       INTEGER NOT NULL,

    FOREIGN KEY("challenge") REFERENCES "Challenge"("rowid")
);

CREATE TABLE IF NOT EXISTS "CTF" (
    "rowid"         INTEGER PRIMARY KEY,
    "name"          TEXT NOT NULL,
    "archived"      INTEGER NOT NULL,
    "start_time"    INTEGER NOT NULL,
    "end_time"      INTEGER,
    "slack_channel_id"  TEXT NOT NULL UNIQUE
);
COMMIT;
