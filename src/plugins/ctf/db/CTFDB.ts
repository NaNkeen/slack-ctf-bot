import { Result, ok, err } from "neverthrow";
import { CTF, CTFId } from "../CTF";
import { Challenge } from "../Challenge";
import { SlackUser } from "../../../slack_interface/user";
import { SlackChannel } from "../../../slack_interface/channel";
import { getEpochTime } from "../../../core/utils";
import { logger } from "../../../core/config";
import {
    PrismaClient,
    CtfProps,
    ChallengeProps,
    WorkerProps,
    CredsProp,
} from "@prisma/client";

//TODO: Maybe make this an interface if we need a differnt backing store?
export class CTFDB {
    prisma: PrismaClient;

    private constructor(prisma: PrismaClient) {
        this.prisma = prisma;
    }

    public static setup(): Result<CTFDB, Error> {
        return ok(new CTFDB(new PrismaClient()));
    }

    public async getCtfPropsById(id: CTFId): Promise<Result<CtfProps, Error>> {
        const ctfProps = await this.prisma.ctfProps.findOne({
            where: { rowid: id },
        });

        if (ctfProps) {
            return ok(ctfProps);
        } else {
            return err(Error(`No ctf found with id ${id}!`));
        }
    }

    public async newCtfProps(
        name: string,
        generalChannel: SlackChannel
    ): Promise<Result<CtfProps, Error>> {
        try {
            const newCtfProps = await this.prisma.ctfProps.create({
                data: {
                    name: name,
                    archived: 0,
                    startTime: getEpochTime(),
                    endTime: null,
                    channelId: generalChannel.getId(),
                },
            });

            return ok(newCtfProps);
        } catch (error) {
            logger.error(`Can't create CTF with name ${name}`, error);
            return err(error);
        }
    }

    public async newChallengeProps(
        name: string,
        challengeType: string,
        ctf: CTF,
        channel: SlackChannel
    ): Promise<Result<ChallengeProps, Error>> {
        try {
            const newChallengeProps: ChallengeProps = await this.prisma.challengeProps.create(
                {
                    data: {
                        name: name,
                        channelId: channel.getId(),
                        type: challengeType,
                        ctf: {
                            connect: {
                                rowid: ctf.rowid,
                            },
                        },
                    },
                }
            );

            return ok(newChallengeProps);
        } catch (error) {
            logger.error(
                `Got error creating new challenge with name ${name} in ctf ${ctf.name}`,
                error
            );
            return err(error);
        }
    }

    public async getCtfChallengeByName(
        ctf: CTF,
        name: string
    ): Promise<Result<ChallengeProps, Error>> {
        const challengeProps = await this.prisma.challengeProps.findOne({
            where: {
                ctfId_name: {
                    ctfId: ctf.rowid,
                    name: name,
                },
            },
        });

        if (challengeProps) {
            return ok(challengeProps);
        } else {
            return err(
                Error(`No challenge found for ctf ${ctf.name} named ${name}`)
            );
        }
    }

    public async addWorker(
        challenge: Challenge,
        user: SlackUser
    ): Promise<Result<WorkerProps, Error>> {
        try {
            const workerProps = await this.prisma.workerProps.create({
                data: {
                    chatId: user.id,
                    solved: 0,
                    challenge: {
                        connect: {
                            rowid: challenge.rowid,
                        },
                    },
                },
            });
            return ok(workerProps);
        } catch (error) {
            return err(error);
        }
    }

    public async getWorkerProps(
        challenge: Challenge
    ): Promise<Array<WorkerProps>> {
        const workerProps: WorkerProps[] = await this.prisma.workerProps.findMany(
            {
                where: {
                    challengeId: challenge.rowid,
                },
            }
        );

        return workerProps;
    }

    //TODO: This incurrs horrble round trip penalties, and I can find
    // no immediate easy solution online to fix this properly. Why are
    // all the libs for JS/TS so lacking? Maybe look into changing this backend
    // to a different store, or even writing my own SQL lib.
    //
    // NOTE: I think batch updates via transacations might be a good idea when it gets merged:
    // https://github.com/prisma/prisma-client-js/issues/349
    //
    /**
     * Sets solvers on a challenge, each Solver doesn't have to have already been a Worker
     */
    public async markSolved(
        challenge: Challenge,
        workers: Array<SlackUser>
    ): Promise<void> {
        const promises = workers.map(async (solver) => {
            await this.prisma.workerProps.upsert({
                create: {
                    chatId: solver.id,
                    challenge: {
                        connect: {
                            rowid: challenge.rowid,
                        },
                    },
                    solved: 1,
                },
                update: {
                    solved: 1,
                },
                where: {
                    challengeId_chatId: {
                        challengeId: challenge.rowid,
                        chatId: solver.id,
                    },
                },
            });
        });

        await Promise.all(promises);
    }

    public async markUnsolved(challenge: Challenge): Promise<void> {
        await this.prisma.workerProps.updateMany({
            data: {
                solved: 0,
            },
            where: {
                challengeId: challenge.rowid,
            },
        });
    }

    public async archiveCtf(ctf: CTF): Promise<void> {
        await this.prisma.ctfProps.update({
            data: {
                archived: 1,
            },
            where: {
                rowid: ctf.rowid,
            },
        });
    }

    public async getCredsPropForCtf(
        ctf: CtfProps
    ): Promise<Result<CredsProp, Error>> {
        try {
            const props = await this.prisma.credsProp.findOne({
                where: {
                    ctfId: ctf.rowid,
                },
            });

            if (props == null) {
                return err(Error());
            }

            return ok(props);
        } catch (error) {
            logger.error(`Failed to get all creds for ctf ${ctf.name}`, error);
            return err(error);
        }
    }

    public async setCredsPropForCtf(
        ctf: CtfProps,
        username: string,
        password: string
    ): Promise<Result<CredsProp, Error>> {
        try {
            const props = await this.prisma.credsProp.upsert({
                where: {
                    ctfId: ctf.rowid,
                },
                update: {
                    username: username,
                    password: password,
                },
                create: {
                    username: username,
                    password: password,
                    ctf: {
                        connect: {
                            rowid: ctf.rowid,
                        },
                    },
                },
            });

            if (props == null) {
                return err(Error());
            }

            return ok(props);
        } catch (error) {
            logger.error(`Failed to get all creds for ctf ${ctf.name}`, error);
            return err(error);
        }
    }

    public async getChallengeProps(
        ctf: CtfProps
    ): Promise<Array<ChallengeProps>> {
        const challengeProps: ChallengeProps[] = await this.prisma.challengeProps.findMany(
            {
                where: {
                    ctfId: ctf.rowid,
                },
            }
        );

        return challengeProps;
    }

    public async getAllCtfProps(
        archived = false
    ): Promise<Result<Array<CtfProps>, Error>> {
        try {
            return ok(
                await this.prisma.ctfProps.findMany({
                    where: {
                        archived: archived ? 1 : 0,
                    },
                })
            );
        } catch (error) {
            logger.error(`Failed to get all ctfs`, error);
            return err(error);
        }
    }

    public async getChallengePropsFromChannel(
        channel: SlackChannel
    ): Promise<Result<ChallengeProps, Error>> {
        try {
            const challProp = await this.prisma.challengeProps.findOne({
                where: {
                    channelId: channel.getId(),
                },
            });
            if (!challProp) {
                return err(
                    Error(
                        `Couldn't find challenge from channel: ${channel.getName()}`
                    )
                );
            }

            return ok(challProp);
        } catch (error) {
            return err(error);
        }
    }

    public async getOwningCtfProps(
        challenge: Challenge
    ): Promise<Result<CtfProps, Error>> {
        const ctfProp = await this.prisma.challengeProps.findOne({
            where: {
                rowid: challenge.rowid,
            },
            include: {
                ctf: true,
            },
        });

        if (!ctfProp) {
            return err(Error(`No ctf found for challenge ${challenge.name}`));
        }

        return ok(ctfProp.ctf);
    }

    public async getCtfFromGeneralChannel(
        channel: SlackChannel
    ): Promise<Result<CtfProps, Error>> {
        try {
            const props = await this.prisma.ctfProps.findOne({
                where: {
                    channelId: channel.getId(),
                },
            });

            if (!props) {
                return err(
                    Error(
                        `Failed to find ctf from general channel ${channel.getName()}`
                    )
                );
            }

            return ok(props);
        } catch (error) {
            return err(error);
        }
    }
}
