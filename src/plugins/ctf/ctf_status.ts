import { Challenge } from "./Challenge";
import { SlackAPI } from "../../slack_interface/slack_api";
import { Result } from "neverthrow";
import { SlackUser } from "../../slack_interface/user";
import { logger } from "../../core/config";
import { CTF } from "./CTF";
import { SlackChannel } from "../../slack_interface/channel";
import { ChallengeWorker } from "./ChallengeWorker";

export async function getSolvedStatus(
    challenge: Challenge,
    api: SlackAPI
): Promise<string> {
    const slackSolvers: Array<Result<SlackUser, Error>> = await Promise.all(
        (await challenge.solvers()).map((solver: ChallengeWorker) =>
            api.getUserById(solver.chatId)
        )
    );

    const solverString = slackSolvers
        .map((slackSolverOrErr: Result<SlackUser, Error>) => {
            return slackSolverOrErr.match(
                (slackSolver) => slackSolver.real_name,
                (error) => {
                    logger.error("Error getting slack user in status", error);
                    return "[Unknown User]";
                }
            );
        })
        .join(", ");

    return `:tada: *${challenge.name}* ${challenge.type} (Solved by: ${solverString})`;
}

export async function getWorkerStatus(
    challenge: Challenge,
    api: SlackAPI
): Promise<string> {
    const workersOrErr: Array<Result<SlackUser, Error>> = await Promise.all(
        (await challenge.getWorkers()).map((worker: ChallengeWorker) =>
            api.getUserById(worker.chatId)
        )
    );

    const workers = workersOrErr.reduce((result: Array<SlackUser>, worker) => {
        if (worker.isOk()) {
            return result.concat(worker.value);
        } else {
            const error = worker.error;
            logger.error("Error getting slack user in status", error);
            return result;
        }
    }, []);

    const workerString = workers
        .map((worker: SlackUser) => worker.real_name)
        .join(", ");

    return `[${workers.length} active] *${challenge.name}* ${challenge.type}: ${workerString}`;
}

export async function getChallengeStatus(
    challenge: Challenge,
    api: SlackAPI
): Promise<string> {
    if (await challenge.isSolved()) {
        return getSolvedStatus(challenge, api);
    } else {
        return getWorkerStatus(challenge, api);
    }
}

export async function getCtfStatus(
    ctf: CTF,
    api: SlackAPI
): Promise<Result<string, Error>> {
    const challenges = await ctf.getChallenges();

    const challengeStatuses: Array<string> = await Promise.all(
        challenges.map(async (challenge) => getChallengeStatus(challenge, api))
    );

    const generalChannel: Result<
        SlackChannel,
        Error
    > = await api.getChannelById(ctf.getGeneralChannel());

    return generalChannel.map((channel) => {
        return (
            `============= ${channel.getLink()} =============\n` +
            (challengeStatuses.length == 0
                ? "No challenges!"
                : challengeStatuses.join("\n"))
        );
    });
}
