import { CTFDB } from "./db/CTFDB";
import { logger } from "../../core/config";
import { Result, ok } from "neverthrow";
import { Challenge } from "./Challenge";
import { ChannelId } from "../../slack_interface/rtm_types";
import { SlackChannel } from "../../slack_interface/channel";
import { CtfProps } from "@prisma/client";

export type CTFId = number;

/**
 * CTF model wrapper.
 *
 * NOTE: This has a private constructor, this is so that the only way to make
 * this type is via our guard static methods which ensure we are coming from a DB backend.
 * Whilst this is roundabout, this ensures one import part - if we present this type to a function we
 * *must* have a valid DB backing for this class so we don't need to faff about checking for existence or no.
 *
 * This is followed by the other "models" in the CTF plugin.
 */
export class CTF implements CtfProps {
    private db: CTFDB;
    props: CtfProps;

    private constructor(db: CTFDB, props: CtfProps) {
        this.db = db;
        this.props = props;
    }

    public static async getCtfFromId(
        db: CTFDB,
        id: CTFId
    ): Promise<Result<CTF, Error>> {
        const props = await db.getCtfPropsById(id);

        return props.map((props) => {
            return new this(db, props);
        });
    }

    public static async newCtf(
        db: CTFDB,
        name: string,
        generalChannel: SlackChannel
    ): Promise<Result<CTF, Error>> {
        const propsOrErr = await db.newCtfProps(name, generalChannel);

        return propsOrErr.map((props) => {
            return new this(db, props);
        });
    }

    public static async getCtfs(
        db: CTFDB,
        archived = false
    ): Promise<Result<CTF[], Error>> {
        return (await db.getAllCtfProps(archived)).map((ctfProps) => {
            return ctfProps.map((ctfProp) => {
                return new this(db, ctfProp);
            });
        });
    }

    public static async getOwningCtf(
        db: CTFDB,
        challenge: Challenge
    ): Promise<Result<CTF, Error>> {
        return (await db.getOwningCtfProps(challenge)).map((props) => {
            return new this(db, props);
        });
    }

    public static async fromGeneralChannel(
        db: CTFDB,
        channel: SlackChannel
    ): Promise<Result<CTF, Error>> {
        return (await db.getCtfFromGeneralChannel(channel)).map((ctfProps) => {
            return new this(db, ctfProps);
        });
    }

    public getChallenges(): Promise<Array<Challenge>> {
        return Challenge.getChallenges(this.db, this);
    }

    public getChallengeByName(name: string): Promise<Result<Challenge, Error>> {
        return Challenge.getChallenge(this.db, this, name);
    }

    public async archive(): Promise<Result<null, Error>> {
        logger.info(`Archiving ctf: "${this.name}"`);

        await this.db.archiveCtf(this);
        return ok(null);
    }

    public getGeneralChannel(): ChannelId {
        return this.props.channelId;
    }

    public isArchived(): boolean {
        return this.props.archived == 1;
    }

    public async addChallenge(
        name: string,
        challengeType: string,
        channel: SlackChannel
    ): Promise<Result<Challenge, Error>> {
        return Challenge.addChallenge(
            this.db,
            name,
            challengeType,
            this,
            channel
        );
    }

    get name(): string {
        return this.props.name;
    }

    get archived(): number {
        return this.props.archived;
    }

    get rowid(): CTFId {
        return this.props.rowid;
    }

    get endTime(): number | null {
        return this.props.endTime;
    }

    get channelId(): string {
        return this.props.channelId;
    }

    get startTime(): number {
        return this.props.startTime;
    }
}
