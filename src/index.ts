import dotenv from "dotenv";

import { Bot } from "./core/bot";
import { logger } from "./core/config";
import { MessageEvent } from "./slack_interface/rtm_types";
import { SlackAPI } from "./slack_interface/slack_api";
import { CTFCommands } from "./plugins/ctf/ctf_bot";
import { CTFDB } from "./plugins/ctf/db/CTFDB";
import { createEventAdapter } from "@slack/events-api";
import SlackEventAdapter from "@slack/events-api/dist/adapter";
import { DefaultCommands } from "./plugins/default/default_cmds";

dotenv.config();
const TOKEN = process.env.SLACK_TOKEN;
const SIGNING_SECRET = process.env.SIGNING_SECRET;
const LISTEN_PORT_ENV = process.env.LISTEN_PORT;
const default_port = 3000;

if (!TOKEN || !SIGNING_SECRET) {
    logger.warn("NEED A SLACK API TOKEN!!!!");
    process.exit();
}

let listen_port = 0;
if(!LISTEN_PORT_ENV) {
    logger.warn(`No listen port, default to ${default_port}`);
    listen_port = default_port;
}else {
    listen_port = Number(LISTEN_PORT_ENV);
}

const slackAPI = SlackAPI.init(TOKEN);
const bot = new Bot(slackAPI);

const defaultCommands = new DefaultCommands(bot);
defaultCommands.init(bot);

// TODO: Up here in the bot we shouldn't know about this db, move into something in the plugin structure
const dbOrErr = CTFDB.setup();

if (dbOrErr.isErr()) {
    logger.error(`Failed to setup db`, dbOrErr.error);
    process.exit(1);
}

const db = dbOrErr.value;
const ctfCommands = new CTFCommands(db);
ctfCommands.init(bot);

// Initialize the adapter to trigger listeners with the respond function
const slackEvents: SlackEventAdapter = createEventAdapter(SIGNING_SECRET, {});

slackEvents.on("message", (event: MessageEvent) => {
    if (event.bot_id != null) {
        // Don't process bot messages, this includes our own
        return;
    }
    void bot.parseEvent(event);
});

slackEvents.on("error", (error: Error) => {
    logger.error("Events received error", error);
});

const main = async (): Promise<void> => {
    logger.info("Starting Slack event listener");

    try {
        await slackEvents.start(listen_port);
        logger.info(`Running event listener on port ${listen_port}`);
    } catch (e) {
        logger.error(
            "Slack Real Time Messaging failed for some reason, who knows?",
            e
        );
    }
};

main().catch((error) => {
    logger.error("Failed to start main", error);
});
